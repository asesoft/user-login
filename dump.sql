--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-1.pgdg18.04+1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: system; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA system;


ALTER SCHEMA system OWNER TO postgres;

--
-- Name: user; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "user";


ALTER SCHEMA "user" OWNER TO postgres;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: users_insert_settings(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.users_insert_settings() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
    INSERT INTO "user"."user_settings" (user_id, email_active, first_login, gauth) VALUES(NEW.user_id, FALSE, FALSE, FALSE);
    RETURN NEW;
END;
$$;


ALTER FUNCTION public.users_insert_settings() OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: logs; Type: TABLE; Schema: system; Owner: postgres
--

CREATE TABLE system.logs (
    log_id bigint NOT NULL,
    user_id bigint NOT NULL,
    type integer,
    ip character varying(128) NOT NULL,
    date timestamp without time zone,
    platform json NOT NULL,
    data json
);


ALTER TABLE system.logs OWNER TO postgres;

--
-- Name: logs_log_id_seq; Type: SEQUENCE; Schema: system; Owner: postgres
--

CREATE SEQUENCE system.logs_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE system.logs_log_id_seq OWNER TO postgres;

--
-- Name: logs_log_id_seq; Type: SEQUENCE OWNED BY; Schema: system; Owner: postgres
--

ALTER SEQUENCE system.logs_log_id_seq OWNED BY system.logs.log_id;


--
-- Name: auth_tokens; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".auth_tokens (
    token_id bigint NOT NULL,
    user_id bigint,
    token character varying(128),
    expiry_date timestamp without time zone
);


ALTER TABLE "user".auth_tokens OWNER TO postgres;

--
-- Name: auth_tokens_token_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".auth_tokens_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".auth_tokens_token_id_seq OWNER TO postgres;

--
-- Name: auth_tokens_token_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".auth_tokens_token_id_seq OWNED BY "user".auth_tokens.token_id;


--
-- Name: change_password_codes; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".change_password_codes (
    code_id bigint NOT NULL,
    user_id bigint,
    code character varying(128)
);


ALTER TABLE "user".change_password_codes OWNER TO postgres;

--
-- Name: change_password_codes_code_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".change_password_codes_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".change_password_codes_code_id_seq OWNER TO postgres;

--
-- Name: change_password_codes_code_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".change_password_codes_code_id_seq OWNED BY "user".change_password_codes.code_id;


--
-- Name: email_verification_codes; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".email_verification_codes (
    code_id bigint NOT NULL,
    user_id bigint NOT NULL,
    code character varying(128) NOT NULL
);


ALTER TABLE "user".email_verification_codes OWNER TO postgres;

--
-- Name: email_verification_codes_code_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".email_verification_codes_code_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".email_verification_codes_code_id_seq OWNER TO postgres;

--
-- Name: email_verification_codes_code_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".email_verification_codes_code_id_seq OWNED BY "user".email_verification_codes.code_id;


--
-- Name: gauth_secrets; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".gauth_secrets (
    secret_id bigint NOT NULL,
    user_id bigint NOT NULL,
    secret character varying(128) NOT NULL,
    qr_code text
);


ALTER TABLE "user".gauth_secrets OWNER TO postgres;

--
-- Name: gauth_secrets_secret_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".gauth_secrets_secret_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".gauth_secrets_secret_id_seq OWNER TO postgres;

--
-- Name: gauth_secrets_secret_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".gauth_secrets_secret_id_seq OWNED BY "user".gauth_secrets.secret_id;


--
-- Name: user_groups; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".user_groups (
    group_id bigint NOT NULL,
    name character varying(100),
    level smallint NOT NULL,
    permissions json
);


ALTER TABLE "user".user_groups OWNER TO postgres;

--
-- Name: user_groups_group_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".user_groups_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".user_groups_group_id_seq OWNER TO postgres;

--
-- Name: user_groups_group_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".user_groups_group_id_seq OWNED BY "user".user_groups.group_id;


--
-- Name: user_settings; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".user_settings (
    settings_id bigint NOT NULL,
    user_id bigint NOT NULL,
    email_active boolean NOT NULL,
    first_login boolean NOT NULL,
    gauth boolean NOT NULL
);


ALTER TABLE "user".user_settings OWNER TO postgres;

--
-- Name: user_settings_settings_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".user_settings_settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".user_settings_settings_id_seq OWNER TO postgres;

--
-- Name: user_settings_settings_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".user_settings_settings_id_seq OWNED BY "user".user_settings.settings_id;


--
-- Name: users; Type: TABLE; Schema: user; Owner: postgres
--

CREATE TABLE "user".users (
    user_id bigint NOT NULL,
    group_id bigint NOT NULL,
    name character varying(100) NOT NULL,
    surname character varying(50),
    email character varying(100) NOT NULL,
    password character varying(32) NOT NULL
);


ALTER TABLE "user".users OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: user; Owner: postgres
--

CREATE SEQUENCE "user".users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "user".users_user_id_seq OWNER TO postgres;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: user; Owner: postgres
--

ALTER SEQUENCE "user".users_user_id_seq OWNED BY "user".users.user_id;


--
-- Name: logs log_id; Type: DEFAULT; Schema: system; Owner: postgres
--

ALTER TABLE ONLY system.logs ALTER COLUMN log_id SET DEFAULT nextval('system.logs_log_id_seq'::regclass);


--
-- Name: auth_tokens token_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".auth_tokens ALTER COLUMN token_id SET DEFAULT nextval('"user".auth_tokens_token_id_seq'::regclass);


--
-- Name: change_password_codes code_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".change_password_codes ALTER COLUMN code_id SET DEFAULT nextval('"user".change_password_codes_code_id_seq'::regclass);


--
-- Name: email_verification_codes code_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".email_verification_codes ALTER COLUMN code_id SET DEFAULT nextval('"user".email_verification_codes_code_id_seq'::regclass);


--
-- Name: gauth_secrets secret_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".gauth_secrets ALTER COLUMN secret_id SET DEFAULT nextval('"user".gauth_secrets_secret_id_seq'::regclass);


--
-- Name: user_groups group_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".user_groups ALTER COLUMN group_id SET DEFAULT nextval('"user".user_groups_group_id_seq'::regclass);


--
-- Name: user_settings settings_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".user_settings ALTER COLUMN settings_id SET DEFAULT nextval('"user".user_settings_settings_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".users ALTER COLUMN user_id SET DEFAULT nextval('"user".users_user_id_seq'::regclass);


--
-- Data for Name: logs; Type: TABLE DATA; Schema: system; Owner: postgres
--

COPY system.logs (log_id, user_id, type, ip, date, platform, data) FROM stdin;
70	92	1	127.0.0.1	2019-01-05 15:17:11.993	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
71	92	4	127.0.0.1	2019-01-05 15:18:04.663	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
72	92	2	127.0.0.1	2019-01-05 15:18:07.42	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
88	108	1	127.0.0.1	2019-01-07 18:02:12.565	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
89	92	2	127.0.0.1	2019-01-07 18:02:16.824	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
90	92	2	127.0.0.1	2019-01-07 18:02:17.841	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
91	92	2	127.0.0.1	2019-01-07 18:02:18.236	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
92	92	2	127.0.0.1	2019-01-07 18:02:18.611	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
93	92	2	127.0.0.1	2019-01-07 18:02:19.033	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
94	92	2	127.0.0.1	2019-01-07 18:02:19.409	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
95	92	2	127.0.0.1	2019-01-07 18:02:19.776	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
96	92	2	127.0.0.1	2019-01-07 18:02:19.961	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
97	92	2	127.0.0.1	2019-01-07 18:02:20.158	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
98	92	2	127.0.0.1	2019-01-07 18:02:20.333	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
99	92	2	127.0.0.1	2019-01-07 18:02:20.521	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
100	92	2	127.0.0.1	2019-01-07 18:02:20.697	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
101	92	2	127.0.0.1	2019-01-07 18:02:20.887	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
102	92	2	127.0.0.1	2019-01-07 18:02:21.064	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
103	92	2	127.0.0.1	2019-01-07 18:02:21.249	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
104	92	2	127.0.0.1	2019-01-07 18:02:21.43	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
105	92	2	127.0.0.1	2019-01-07 18:02:21.625	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
106	92	2	127.0.0.1	2019-01-07 18:02:21.817	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
107	92	2	127.0.0.1	2019-01-07 18:02:21.999	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
108	92	2	127.0.0.1	2019-01-07 18:02:22.182	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
109	92	2	127.0.0.1	2019-01-07 18:02:22.381	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
110	92	2	127.0.0.1	2019-01-07 18:02:22.564	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
111	92	2	127.0.0.1	2019-01-07 18:02:22.751	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
112	92	2	127.0.0.1	2019-01-07 18:02:22.965	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
113	92	2	127.0.0.1	2019-01-07 18:02:23.163	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
114	92	2	127.0.0.1	2019-01-07 18:02:23.361	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
115	92	2	127.0.0.1	2019-01-07 18:02:23.538	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
116	92	2	127.0.0.1	2019-01-07 18:02:23.745	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
117	92	2	127.0.0.1	2019-01-07 18:02:23.963	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
118	92	2	127.0.0.1	2019-01-07 18:02:24.162	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
119	92	2	127.0.0.1	2019-01-07 18:02:24.384	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
120	92	2	127.0.0.1	2019-01-07 18:02:24.569	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
121	92	2	127.0.0.1	2019-01-07 18:02:24.779	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
122	92	2	127.0.0.1	2019-01-07 18:02:24.987	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
123	92	2	127.0.0.1	2019-01-07 18:02:25.215	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
124	92	2	127.0.0.1	2019-01-07 18:02:25.424	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
125	92	2	127.0.0.1	2019-01-07 18:02:25.63	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
126	92	2	127.0.0.1	2019-01-07 18:02:25.991	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
127	92	2	127.0.0.1	2019-01-07 18:02:26.171	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
128	92	2	127.0.0.1	2019-01-07 18:02:26.356	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
129	92	2	127.0.0.1	2019-01-07 18:02:26.488	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
130	92	2	127.0.0.1	2019-01-07 19:23:25.085	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
131	92	2	127.0.0.1	2019-01-07 19:23:25.602	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
132	92	2	127.0.0.1	2019-01-07 19:23:26.064	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
133	92	2	127.0.0.1	2019-01-07 19:23:26.442	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
134	92	2	127.0.0.1	2019-01-07 19:23:26.663	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
135	92	2	127.0.0.1	2019-01-07 19:23:26.895	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
136	92	2	127.0.0.1	2019-01-07 19:23:27.111	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
137	92	2	127.0.0.1	2019-01-07 19:23:32.159	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
138	92	2	127.0.0.1	2019-01-07 19:24:07.102	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
139	92	2	127.0.0.1	2019-01-07 20:06:58.884	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
140	92	2	127.0.0.1	2019-01-07 20:06:59.033	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
141	92	2	127.0.0.1	2019-01-07 20:06:59.201	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
142	92	2	127.0.0.1	2019-01-07 20:06:59.394	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
143	92	2	127.0.0.1	2019-01-07 20:06:59.562	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
144	92	2	127.0.0.1	2019-01-07 20:06:59.739	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
145	92	2	127.0.0.1	2019-01-07 20:06:59.916	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
146	92	2	127.0.0.1	2019-01-07 20:07:00.092	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
147	92	2	127.0.0.1	2019-01-07 20:07:00.279	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
148	92	2	127.0.0.1	2019-01-07 20:07:00.444	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
149	92	2	127.0.0.1	2019-01-07 20:07:00.611	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
150	92	2	127.0.0.1	2019-01-07 20:07:00.783	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
151	92	2	127.0.0.1	2019-01-07 20:07:00.958	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
152	92	2	127.0.0.1	2019-01-07 20:07:01.148	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
153	92	2	127.0.0.1	2019-01-07 20:07:01.318	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
154	92	2	127.0.0.1	2019-01-07 20:07:01.481	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
155	92	2	127.0.0.1	2019-01-07 20:07:01.692	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
156	92	2	127.0.0.1	2019-01-07 20:07:02.045	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
157	92	2	127.0.0.1	2019-01-07 20:07:02.232	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
158	92	2	127.0.0.1	2019-01-07 20:07:02.428	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
159	92	2	127.0.0.1	2019-01-07 20:07:02.599	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
160	92	2	127.0.0.1	2019-01-07 20:07:02.782	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
161	92	2	127.0.0.1	2019-01-07 20:07:02.973	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
162	92	2	127.0.0.1	2019-01-07 20:07:03.154	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
163	92	2	127.0.0.1	2019-01-07 20:07:03.34	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
164	92	2	127.0.0.1	2019-01-07 20:07:03.527	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
165	92	2	127.0.0.1	2019-01-07 20:07:03.715	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
166	92	2	127.0.0.1	2019-01-07 20:07:03.917	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
167	92	2	127.0.0.1	2019-01-07 20:07:04.103	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
168	92	2	127.0.0.1	2019-01-07 20:07:04.297	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
169	92	2	127.0.0.1	2019-01-07 20:07:04.486	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
170	92	2	127.0.0.1	2019-01-07 20:07:04.693	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
171	92	2	127.0.0.1	2019-01-07 20:07:04.893	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
172	92	2	127.0.0.1	2019-01-07 20:07:05.071	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
173	92	2	127.0.0.1	2019-01-07 20:07:05.252	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
174	92	2	127.0.0.1	2019-01-07 20:07:05.584	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
175	92	2	127.0.0.1	2019-01-07 20:07:05.78	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
176	92	2	127.0.0.1	2019-01-07 20:07:05.96	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
177	92	2	127.0.0.1	2019-01-07 20:07:06.135	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
178	92	2	127.0.0.1	2019-01-07 20:07:06.319	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
179	92	2	127.0.0.1	2019-01-07 20:07:06.482	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
180	92	2	127.0.0.1	2019-01-07 20:07:06.646	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
181	92	2	127.0.0.1	2019-01-07 20:07:07.111	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
182	92	2	127.0.0.1	2019-01-07 20:07:07.297	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
183	92	2	127.0.0.1	2019-01-07 20:07:07.496	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
184	92	2	127.0.0.1	2019-01-07 20:07:07.667	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
185	92	2	127.0.0.1	2019-01-07 20:07:07.847	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
186	92	2	127.0.0.1	2019-01-07 20:07:08.028	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
187	92	2	127.0.0.1	2019-01-07 20:07:08.215	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
188	92	2	127.0.0.1	2019-01-07 20:07:08.422	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
189	92	2	127.0.0.1	2019-01-07 20:07:08.616	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
190	92	2	127.0.0.1	2019-01-07 20:07:08.818	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
191	92	2	127.0.0.1	2019-01-07 20:07:09.018	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
192	92	2	127.0.0.1	2019-01-07 20:07:09.217	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
193	92	2	127.0.0.1	2019-01-07 20:07:09.405	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
194	92	2	127.0.0.1	2019-01-07 20:07:09.605	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
195	92	2	127.0.0.1	2019-01-07 20:07:09.809	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
196	92	2	127.0.0.1	2019-01-07 20:07:10.002	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
197	92	2	127.0.0.1	2019-01-07 20:07:10.219	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
198	92	2	127.0.0.1	2019-01-07 20:07:10.421	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
199	92	2	127.0.0.1	2019-01-07 20:07:10.606	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
200	92	2	127.0.0.1	2019-01-07 20:07:10.802	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
201	92	2	127.0.0.1	2019-01-07 20:07:11	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
202	92	2	127.0.0.1	2019-01-07 20:07:11.202	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
203	92	2	127.0.0.1	2019-01-07 20:07:11.392	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
204	92	2	127.0.0.1	2019-01-07 20:07:11.594	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
205	92	2	127.0.0.1	2019-01-07 20:07:11.785	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
206	92	2	127.0.0.1	2019-01-07 20:07:12.006	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
207	92	2	127.0.0.1	2019-01-07 20:07:12.209	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
208	92	2	127.0.0.1	2019-01-07 20:07:12.415	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
209	92	2	127.0.0.1	2019-01-07 20:07:12.61	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
210	92	2	127.0.0.1	2019-01-07 20:07:12.809	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
211	92	2	127.0.0.1	2019-01-07 20:07:13.01	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
212	92	2	127.0.0.1	2019-01-07 20:07:13.208	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
213	92	2	127.0.0.1	2019-01-07 20:07:13.412	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
214	92	2	127.0.0.1	2019-01-07 20:07:13.606	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
215	92	2	127.0.0.1	2019-01-07 20:07:13.82	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
216	92	2	127.0.0.1	2019-01-07 20:07:14.019	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
217	92	2	127.0.0.1	2019-01-07 20:07:14.222	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
218	92	2	127.0.0.1	2019-01-07 20:07:14.426	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
219	92	2	127.0.0.1	2019-01-07 20:07:14.633	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
220	92	2	127.0.0.1	2019-01-07 20:07:14.811	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
221	92	2	127.0.0.1	2019-01-07 20:07:15.027	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
222	92	2	127.0.0.1	2019-01-07 20:07:15.214	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
223	92	2	127.0.0.1	2019-01-07 20:07:15.414	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
224	92	2	127.0.0.1	2019-01-07 20:07:15.601	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
225	92	2	127.0.0.1	2019-01-07 20:07:15.791	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
226	92	2	127.0.0.1	2019-01-07 20:07:15.971	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
227	92	2	127.0.0.1	2019-01-07 20:07:16.163	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
228	92	2	127.0.0.1	2019-01-07 20:07:16.358	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
229	92	2	127.0.0.1	2019-01-07 20:07:16.534	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
230	92	2	127.0.0.1	2019-01-07 20:07:16.736	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
231	92	2	127.0.0.1	2019-01-07 20:07:16.937	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
232	92	2	127.0.0.1	2019-01-07 20:07:17.122	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
233	92	2	127.0.0.1	2019-01-07 20:07:17.289	{"isAuthoritative":true,"isMobile":false,"isTablet":false,"isiPad":false,"isiPod":false,"isiPhone":false,"isAndroid":false,"isBlackberry":false,"isOpera":false,"isIE":false,"isEdge":false,"isIECompatibilityMode":false,"isSafari":false,"isFirefox":false,"isWebkit":false,"isChrome":true,"isKonqueror":false,"isOmniWeb":false,"isSeaMonkey":false,"isFlock":false,"isAmaya":false,"isPhantomJS":false,"isEpiphany":false,"isDesktop":true,"isWindows":false,"isLinux":true,"isLinux64":true,"isMac":false,"isChromeOS":false,"isBada":false,"isSamsung":false,"isRaspberry":false,"isBot":false,"isCurl":false,"isAndroidTablet":false,"isWinJs":false,"isKindleFire":false,"isSilk":false,"isCaptive":false,"isSmartTV":false,"isUC":false,"isFacebook":false,"isAlamoFire":false,"silkAccelerated":false,"browser":"Chrome","version":"71.0.3578.98","os":"Linux 64","platform":"Linux","geoIp":{},"source":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"}	\N
\.


--
-- Data for Name: auth_tokens; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".auth_tokens (token_id, user_id, token, expiry_date) FROM stdin;
86	92	13befc79-6784-478c-a546-c5bb2760707e	2019-01-21 20:06:58.879
87	92	768ee8e0-1eca-4596-80e5-fcb3c4b7ec2f	2019-01-21 20:06:59.031
88	92	a5efaaf1-f0c7-49d8-a611-29048a0fd3d6	2019-01-21 20:06:59.199
89	92	02dc439f-67cd-489d-bf56-58f1384093cb	2019-01-21 20:06:59.391
90	92	e00f634a-b113-431c-8dfc-42592428cc6b	2019-01-21 20:06:59.559
91	92	62eb1ca7-df69-4275-8c21-217d9a929e21	2019-01-21 20:06:59.737
92	92	5106d0b5-c2bd-49e6-9d78-5b4eea3c1fc5	2019-01-21 20:06:59.915
93	92	d8d88849-5a9e-4ecb-b758-811e12687510	2019-01-21 20:07:00.089
94	92	27242a42-5582-40b7-b1a5-82b79a6c71e1	2019-01-21 20:07:00.276
95	92	6219811c-1288-4614-a6a0-38b3d15ba0c3	2019-01-21 20:07:00.441
96	92	43eb6f94-162d-4583-9a91-6db3042cbe14	2019-01-21 20:07:00.607
97	92	32d7adfc-0406-463c-8aaa-24f1fc25e59d	2019-01-21 20:07:00.78
98	92	c585043d-c0cf-4b9a-984a-27cc7720e02c	2019-01-21 20:07:00.955
99	92	56958a9b-66f1-418a-b812-2c72f33b9ca9	2019-01-21 20:07:01.146
100	92	4cd691f2-9aef-4222-8a05-a06fd3f4ed13	2019-01-21 20:07:01.315
101	92	85dbdfd7-cfe4-459d-9b94-430015d34e83	2019-01-21 20:07:01.48
102	92	63b0e05b-d859-4886-ac2d-6e6ef3454d1d	2019-01-21 20:07:01.69
103	92	a2c2bee9-3c98-4fd6-907d-2510e399cfa7	2019-01-21 20:07:02.041
104	92	a3a01601-bd9f-466c-8f00-896ecdb63d02	2019-01-21 20:07:02.23
105	92	06511807-753e-4d40-be14-0b5053aabd7d	2019-01-21 20:07:02.424
106	92	f421cd31-63c6-409d-8bfa-fe852c64f673	2019-01-21 20:07:02.596
107	92	f4d8211e-d1c9-4261-93f7-9b1b2d70a5e9	2019-01-21 20:07:02.779
108	92	67c35359-73b3-466c-ad69-a5d45e1b1780	2019-01-21 20:07:02.969
109	92	e29f0285-7461-4f08-9b5a-99818f423a20	2019-01-21 20:07:03.151
110	92	af9274c3-1e4a-4bb7-aaa5-380f9e6f710b	2019-01-21 20:07:03.337
111	92	26527a96-9ecc-43ec-ae20-6fb8638f154c	2019-01-21 20:07:03.525
112	92	45a37766-327a-4855-bb05-dd28b4bd6386	2019-01-21 20:07:03.712
113	92	ada4eacc-09d0-4307-9e5b-38fe2b3ac27f	2019-01-21 20:07:03.914
114	92	df9f75df-5326-47c0-9e88-b5c2d4fd6089	2019-01-21 20:07:04.1
115	92	d35d5db7-03cf-46c7-ac13-249f3d02bc48	2019-01-21 20:07:04.293
116	92	8c071b9d-3bab-4377-9d2c-80a218b94027	2019-01-21 20:07:04.485
117	92	4943c1a0-5c50-4361-a634-130ac3ee0031	2019-01-21 20:07:04.689
118	92	6d94f5d8-a9e1-4bb6-a0cc-62cad3d70c0a	2019-01-21 20:07:04.888
119	92	98209fe5-9961-4409-ab12-922067dc58a7	2019-01-21 20:07:05.068
120	92	62752550-5ea9-4193-a098-87a39c54507b	2019-01-21 20:07:05.248
121	92	b84706fc-c51e-453d-94a4-5a5caa7ace5a	2019-01-21 20:07:05.581
122	92	422feb47-5b31-49b0-9469-2590432205c5	2019-01-21 20:07:05.777
123	92	6059c845-683e-4c91-a91b-107d7a7fd774	2019-01-21 20:07:05.958
124	92	a0427957-6896-4b84-8ed9-755f6dc059f5	2019-01-21 20:07:06.132
125	92	ae0f2a5a-f216-451f-84bd-e07c43b9a2c3	2019-01-21 20:07:06.316
126	92	d9bbfcc0-a16e-42e2-92f8-8c2023a0c0a5	2019-01-21 20:07:06.48
127	92	1bedf080-dd20-4521-af37-73909b1c863a	2019-01-21 20:07:06.643
128	92	5f70bef6-3092-40b3-aada-ad4fea353479	2019-01-21 20:07:07.108
129	92	8b48a849-c6c4-4129-95a1-72c7916add56	2019-01-21 20:07:07.294
130	92	9e8cc6be-e82c-4d54-882e-b0974f90b072	2019-01-21 20:07:07.494
131	92	05ad535d-8795-4044-b911-70145400c63d	2019-01-21 20:07:07.664
132	92	33a4d7a3-e848-41c3-8e5c-b43e09b41a2d	2019-01-21 20:07:07.844
133	92	acbffab3-76c5-4d24-932d-be5ebd8b4ec5	2019-01-21 20:07:08.025
134	92	48802200-6a24-43cc-bbaa-55aac1142a7e	2019-01-21 20:07:08.212
135	92	d39d06b2-8258-4b95-8631-9d22ba1ea606	2019-01-21 20:07:08.42
136	92	53a283b8-8ef0-4c15-af68-96d409fb4621	2019-01-21 20:07:08.612
137	92	c4dbe911-23a6-4984-b488-aa80d6c21cb0	2019-01-21 20:07:08.815
138	92	fd26b1c0-c5b9-4831-93c2-7ea50e601867	2019-01-21 20:07:09.015
139	92	7edc2c68-e05b-4fdc-a583-75ee5a163b02	2019-01-21 20:07:09.214
140	92	2d8e78f4-0957-4363-a683-e21a8a267b75	2019-01-21 20:07:09.399
141	92	d1360d9a-a211-4b55-b8d4-6abd2f364e47	2019-01-21 20:07:09.602
142	92	9bdb122b-597b-4993-9895-8ed466330a5c	2019-01-21 20:07:09.806
143	92	edfb47a1-8961-4346-83f7-eb444fe99b5f	2019-01-21 20:07:09.996
144	92	80030d78-65eb-4f95-83f6-ab2204993eed	2019-01-21 20:07:10.217
145	92	fbad64d8-b589-4fd6-b1f0-6514ceb70ea2	2019-01-21 20:07:10.418
146	92	041b9fc5-d124-4497-beb8-c0122f3298e0	2019-01-21 20:07:10.603
147	92	09c37119-6693-4a94-bf8f-844541a93934	2019-01-21 20:07:10.799
148	92	31a366aa-a831-44b6-ae1e-1a5eae535b1b	2019-01-21 20:07:10.997
149	92	5a39b0c8-3800-426e-8139-1ac3437b3e7c	2019-01-21 20:07:11.2
150	92	8e9e8bd6-f989-4bd7-b2fa-115aca80e629	2019-01-21 20:07:11.389
151	92	f7941a17-5dc1-4bf3-9e54-f93e0e9ba42b	2019-01-21 20:07:11.591
152	92	2c31ee4e-4c43-4c30-b5ba-5aa29601976a	2019-01-21 20:07:11.782
153	92	eb17f680-cf24-4654-9685-9b62f4ae97dd	2019-01-21 20:07:12.002
154	92	919cac70-5698-40bb-ab7c-208a4f53ab46	2019-01-21 20:07:12.204
155	92	04b6e061-4951-4f30-9022-46ca61e391e2	2019-01-21 20:07:12.413
156	92	a1ba8a31-58a7-4fde-b10d-5adf67627207	2019-01-21 20:07:12.606
157	92	f152c749-54d8-4655-9e53-0fb47bf510e5	2019-01-21 20:07:12.807
158	92	0ef4c2a9-3bf9-4e10-a174-a6dc399ec36f	2019-01-21 20:07:13.008
159	92	6053abb9-c017-4c37-bd68-2532e3253ebc	2019-01-21 20:07:13.205
160	92	b9cb9a0c-6a45-407c-bc12-929c69e1078f	2019-01-21 20:07:13.405
161	92	99f08fcb-f65e-4305-84ff-dfe00dade85f	2019-01-21 20:07:13.603
162	92	a9671d49-8738-4e21-87ab-ba7a3a41aa69	2019-01-21 20:07:13.817
163	92	46ed3110-63b8-4a01-baff-889005aaf04d	2019-01-21 20:07:14.015
164	92	b959e7a4-2c4b-49af-a3be-2d575d4e2e41	2019-01-21 20:07:14.219
165	92	2e7fbe9e-8c38-4003-81c7-ca3f08faa1f6	2019-01-21 20:07:14.423
166	92	ff7150dd-2e48-4995-ac98-74ac9ac5c1c3	2019-01-21 20:07:14.631
167	92	8c23fa4a-7a47-41ad-b354-8ae0e7e30630	2019-01-21 20:07:14.809
168	92	5c4a3c22-e804-472d-a56a-2e5247fb0144	2019-01-21 20:07:15.024
169	92	7e81c0a7-8420-4c43-8545-27448a56a6f5	2019-01-21 20:07:15.211
85	92	5f85f96c-b53f-4f23-ad95-73c13342dcdc	2019-01-21 19:24:07.1
170	92	baacddc2-3af5-447a-9ced-d5ce82e753d2	2019-01-21 20:07:15.412
171	92	ed166fb6-a51a-4fb2-a722-4ae1a39d9eb9	2019-01-21 20:07:15.598
172	92	b2451cb5-f384-4c86-85f8-c1e311e92b19	2019-01-21 20:07:15.788
173	92	bff6adb2-a17c-4605-b9e6-f3959fe8615c	2019-01-21 20:07:15.968
174	92	bfae187c-b275-475b-812a-f243c28a2967	2019-01-21 20:07:16.155
175	92	3f54e948-3a1d-4323-a60f-14012ff71795	2019-01-21 20:07:16.355
176	92	7528f595-586f-4f99-bfca-652247d380e9	2019-01-21 20:07:16.531
177	92	f7e2e1be-c341-463b-a542-b981197fc0e0	2019-01-21 20:07:16.734
178	92	d39e9a55-04f1-4016-9911-1e3a5c25f1bf	2019-01-21 20:07:16.934
179	92	f37bba05-68a1-4101-92c3-5f7cad893729	2019-01-21 20:07:17.119
180	92	03ba2fb1-71c2-4b5c-8b4a-54a700d3d222	2019-01-21 20:07:17.286
\.


--
-- Data for Name: change_password_codes; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".change_password_codes (code_id, user_id, code) FROM stdin;
\.


--
-- Data for Name: email_verification_codes; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".email_verification_codes (code_id, user_id, code) FROM stdin;
26	108	6f67de2f-a517-4ef2-89ae-d4c322b77de5
\.


--
-- Data for Name: gauth_secrets; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".gauth_secrets (secret_id, user_id, secret, qr_code) FROM stdin;
28	92	GY6GIRRSKNRFWZSLJ5DEUQ3TJ5YVE6LE	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAAAklEQVR4AewaftIAAAOLSURBVO3BO45jSQADwWRB979y7hhr0CpAeFLPB4yIvzDzv8NMOcyUw0w5zJTDTDnMlMNMOcyUw0w5zJTDTDnMlMNMOcyUw0x58VASfpJKS8ITKu9IQlO5ScJPUnniMFMOM+UwU158mMonJeFGpSXhk5LQVFoSmsqNyicl4ZMOM+UwUw4z5cWXJeEdKu9Iwo1KS0JLQlP5SUl4h8o3HWbKYaYcZsqLv5zKTRK+SeVfcpgph5lymCkv/jFJeIdKS8I7ktBU/maHmXKYKYeZ8uLLVL4pCU3lJgktCTcq36TyJznMlMNMOcyUFx+WhD9JEppKS0JTaUloKk8k4U92mCmHmXKYKS8eUvmdVG5UfieVv8lhphxmymGmvHgoCU2lJaGpvCMJNyo3SWgqTyThiSQ0lZskNJWWhKbyxGGmHGbKYabEX3ggCTcqLQk3KjdJeEKlJeFG5Scl4QmVJw4z5TBTDjPlxUMq71BpSbhJQlO5ScJNEppKS0JLwjtU/maHmXKYKYeZ8uLLktBUmkpLQlP5JJWWhHeovCMJNyrvUGlJ+KTDTDnMlMNMefFhSWgqLQlN5SYJTeVG5QmVJ5LQVG6S0FRaEloSvukwUw4z5TBTXnyYyiepvCMJNyrvSEJTaUloKjdJeIfKTRI+6TBTDjPlMFNePJSEG5WbJDSVloQblRuVdyThJglNpSXhRqUl4SYJP+kwUw4z5TBT4i/8xZLQVFoSfpLKO5Jwo9KScKPyxGGmHGbKYaa8eCgJ71BpSWgqLQlN5R0q70hCU7lJQkvCN6l802GmHGbKYaa8eEjlCZUbld8pCU3lRuUdSXgiCU3licNMOcyUw0x58VASfpLKTRKaSktCU2kqn5SEpvJEEr7pMFMOM+UwU158mMonJeFG5R0qN0m4UXmHyhNJaCrfdJgph5lymCkvviwJ71D5pCT8pCQ8odKS0JJwo/LEYaYcZsphprz4xyShqdwkoam8Q+UmCTcqT6h80mGmHGbKYaa8+Msloam0JDSVmyTcqLQkNJWm0pLwDpWWhJaEpvLEYaYcZsphprz4MpVvUmlJaCotCTcq71BpSWgqN0loKi0JTaUl4ZMOM+UwUw4z5cWHJeEnJeEmCU2lJaEl4UblRqUl4UblHUn4psNMOcyUw0yJvzDzv8NMOcyUw0w5zJTDTDnMlMNMOcyUw0w5zJTDTDnMlMNMOcyUw0z5D44LiCDDohjxAAAAAElFTkSuQmCC
29	108	NZMFGOTGPV5UM6DHFE5EEQZZJY3S433L	data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACECAYAAABRRIOnAAAAAklEQVR4AewaftIAAAOKSURBVO3BQY5rRwIDwWRB979yTi+84GYKEJ7Utr8ZEX8w85fDTDnMlMNMOcyUw0w5zJTDTDnMlMNMOcyUw0w5zJTDTDnMlMNMefFQEn6TSktCU3lHEprKJyXhN6k8cZgph5lymCkvPkzlk5Jwo9KS0FRaEprKE0loKjcqn5SETzrMlMNMOcyUF1+WhHeovCMJN0m4ScKNyjcl4R0q33SYKYeZcpgpL/7lVG6S0FTm/zvMlMNMOcyUF/8xSXiHSktCU/mTHGbKYaYcZsqLL1P5piQ0lRuVloSm0pLQVD5J5Z/kMFMOM+UwU158WBL+S5LQVG6S8E92mCmHmXKYKfEH/yFJaCrvSEJT+ZMcZsphphxmyouHktBUWhKayjuScKPyRBKayjcloancJKGptCQ0lScOM+UwUw4z5cWHJeEmCTcqTeUmCTcqNyqflISm0lRaEprKTRKayicdZsphphxmSvzBL0pCU2lJuFFpSWgq70hCU2lJ+CSVmyQ0ld90mCmHmXKYKS/+ZkloKjdJaCo3SbhRaUloKjdJuFFpSbhRaUl4h8oTh5lymCmHmfLioSTcqDSVdyShqTyh0pLwjiS8Iwk3KjcqN0n4pMNMOcyUw0x58ZDKO5LQVFoSmkpLwjtUnkjCjcpNEm6S0FRaEprKNx1mymGmHGbKi4eScKPyRBKaym9SuUlCU2kq70hCU7lR+aTDTDnMlMNMiT/4oiTcqHxSEp5QuUnCO1RaEm5U3pGEpvLEYaYcZsphpsQfPJCEG5UnknCj8klJaCo3SfgmlZaEG5UnDjPlMFMOM+XFQyrfpHKThKbSktBUnkjCjco7ktBUWhKayjcdZsphphxmyouHkvCbVG6S0FRaEppKU/mkJDSVT0pCU3niMFMOM+UwU158mMonJeFGpSWhJaGp/CaVdyShqdyofNJhphxmymGmvPiyJLxD5QmVmyQ8odKS0JLwSUl4h8oTh5lymCmHmfLiD5eEptKS0FRuktBU3pGEptKS8A6VTzrMlMNMOcyUF/9ySWgqTyThHUm4UblJwhNJaCpPHGbKYaYcZsqLL1P5JpUblZaEG5WbJDSVmyQ0lXckoam0JHzSYaYcZsphpsQfPJCE36TSknCjcpOET1J5RxI+SeWJw0w5zJTDTIk/mPnLYaYcZsphphxmymGmHGbKYaYcZsphphxmymGmHGbKYaYcZsphpvwP20uW/wyrwDQAAAAASUVORK5CYII=
\.


--
-- Data for Name: user_groups; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".user_groups (group_id, name, level, permissions) FROM stdin;
1	Yönetici	1	{"ok":"okAkk"}
3	Editor	2	{"ok": "okokok"}
4	Üye	3	{"ok": "sfgdfg"}
\.


--
-- Data for Name: user_settings; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".user_settings (settings_id, user_id, email_active, first_login, gauth) FROM stdin;
37	108	f	f	f
36	92	t	t	f
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: user; Owner: postgres
--

COPY "user".users (user_id, group_id, name, surname, email, password) FROM stdin;
108	3	Ali Fırat	Güler	fenerim.net@gmail.com	4f185b6ff449a1329fd0fb6fcca42bb7
92	3	ali	veli	ali.firat.guler@gmail.com	4f185b6ff449a1329fd0fb6fcca42bb7
\.


--
-- Name: logs_log_id_seq; Type: SEQUENCE SET; Schema: system; Owner: postgres
--

SELECT pg_catalog.setval('system.logs_log_id_seq', 233, true);


--
-- Name: auth_tokens_token_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".auth_tokens_token_id_seq', 180, true);


--
-- Name: change_password_codes_code_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".change_password_codes_code_id_seq', 3, true);


--
-- Name: email_verification_codes_code_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".email_verification_codes_code_id_seq', 26, true);


--
-- Name: gauth_secrets_secret_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".gauth_secrets_secret_id_seq', 29, true);


--
-- Name: user_groups_group_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".user_groups_group_id_seq', 4, true);


--
-- Name: user_settings_settings_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".user_settings_settings_id_seq', 37, true);


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: user; Owner: postgres
--

SELECT pg_catalog.setval('"user".users_user_id_seq', 108, true);


--
-- Name: logs logs_pkey; Type: CONSTRAINT; Schema: system; Owner: postgres
--

ALTER TABLE ONLY system.logs
    ADD CONSTRAINT logs_pkey PRIMARY KEY (log_id);


--
-- Name: auth_tokens auth_tokens_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".auth_tokens
    ADD CONSTRAINT auth_tokens_pkey PRIMARY KEY (token_id);


--
-- Name: change_password_codes change_password_codes_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".change_password_codes
    ADD CONSTRAINT change_password_codes_pkey PRIMARY KEY (code_id);


--
-- Name: email_verification_codes email_verification_codes_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".email_verification_codes
    ADD CONSTRAINT email_verification_codes_pkey PRIMARY KEY (code_id);


--
-- Name: gauth_secrets gauth_secrets_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".gauth_secrets
    ADD CONSTRAINT gauth_secrets_pkey PRIMARY KEY (secret_id);


--
-- Name: user_groups user_groups_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".user_groups
    ADD CONSTRAINT user_groups_pkey PRIMARY KEY (group_id);


--
-- Name: user_settings user_settings_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".user_settings
    ADD CONSTRAINT user_settings_pkey PRIMARY KEY (settings_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: logs_date_index; Type: INDEX; Schema: system; Owner: postgres
--

CREATE INDEX logs_date_index ON system.logs USING btree (date);


--
-- Name: logs_ip_index; Type: INDEX; Schema: system; Owner: postgres
--

CREATE INDEX logs_ip_index ON system.logs USING btree (ip);


--
-- Name: logs_log_id_uindex; Type: INDEX; Schema: system; Owner: postgres
--

CREATE UNIQUE INDEX logs_log_id_uindex ON system.logs USING btree (log_id);


--
-- Name: logs_type_index; Type: INDEX; Schema: system; Owner: postgres
--

CREATE INDEX logs_type_index ON system.logs USING btree (type);


--
-- Name: logs_user_id_index; Type: INDEX; Schema: system; Owner: postgres
--

CREATE INDEX logs_user_id_index ON system.logs USING btree (user_id);


--
-- Name: auth_tokens_expiry_date_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX auth_tokens_expiry_date_index ON "user".auth_tokens USING btree (expiry_date);


--
-- Name: auth_tokens_token_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX auth_tokens_token_id_uindex ON "user".auth_tokens USING btree (token_id);


--
-- Name: auth_tokens_token_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX auth_tokens_token_uindex ON "user".auth_tokens USING btree (token);


--
-- Name: auth_tokens_user_id_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX auth_tokens_user_id_index ON "user".auth_tokens USING btree (user_id);


--
-- Name: change_password_codes_code_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX change_password_codes_code_id_uindex ON "user".change_password_codes USING btree (code_id);


--
-- Name: change_password_codes_code_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX change_password_codes_code_uindex ON "user".change_password_codes USING btree (code);


--
-- Name: email_verification_codes_code_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX email_verification_codes_code_id_uindex ON "user".email_verification_codes USING btree (code_id);


--
-- Name: email_verification_codes_code_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX email_verification_codes_code_uindex ON "user".email_verification_codes USING btree (code);


--
-- Name: email_verification_codes_user_id_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX email_verification_codes_user_id_index ON "user".email_verification_codes USING btree (user_id);


--
-- Name: gauth_secrets_secret_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX gauth_secrets_secret_id_uindex ON "user".gauth_secrets USING btree (secret_id);


--
-- Name: gauth_secrets_user_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX gauth_secrets_user_id_uindex ON "user".gauth_secrets USING btree (user_id);


--
-- Name: user_configs_email_active_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX user_configs_email_active_index ON "user".user_settings USING btree (email_active);


--
-- Name: user_groups_group_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX user_groups_group_id_uindex ON "user".user_groups USING btree (group_id);


--
-- Name: user_groups_level_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX user_groups_level_uindex ON "user".user_groups USING btree (level);


--
-- Name: user_groups_name_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX user_groups_name_uindex ON "user".user_groups USING btree (name);


--
-- Name: user_settings_settings_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX user_settings_settings_id_uindex ON "user".user_settings USING btree (settings_id);


--
-- Name: user_settings_user_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX user_settings_user_id_uindex ON "user".user_settings USING btree (user_id);


--
-- Name: users_email_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX users_email_uindex ON "user".users USING btree (email);


--
-- Name: users_name_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX users_name_index ON "user".users USING btree (name);


--
-- Name: users_surname_index; Type: INDEX; Schema: user; Owner: postgres
--

CREATE INDEX users_surname_index ON "user".users USING btree (surname);


--
-- Name: users_user_id_uindex; Type: INDEX; Schema: user; Owner: postgres
--

CREATE UNIQUE INDEX users_user_id_uindex ON "user".users USING btree (user_id);


--
-- Name: users users_insert_settings_trigger; Type: TRIGGER; Schema: user; Owner: postgres
--

CREATE TRIGGER users_insert_settings_trigger AFTER INSERT ON "user".users FOR EACH ROW EXECUTE PROCEDURE public.users_insert_settings();


--
-- Name: logs logs_users_user_id_fk; Type: FK CONSTRAINT; Schema: system; Owner: postgres
--

ALTER TABLE ONLY system.logs
    ADD CONSTRAINT logs_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: auth_tokens auth_tokens_users_user_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".auth_tokens
    ADD CONSTRAINT auth_tokens_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: change_password_codes change_password_codes_users_user_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".change_password_codes
    ADD CONSTRAINT change_password_codes_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: email_verification_codes email_verification_codes_users_user_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".email_verification_codes
    ADD CONSTRAINT email_verification_codes_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: gauth_secrets gauth_secrets_users_user_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".gauth_secrets
    ADD CONSTRAINT gauth_secrets_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: user_settings user_configs_users_user_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".user_settings
    ADD CONSTRAINT user_configs_users_user_id_fk FOREIGN KEY (user_id) REFERENCES "user".users(user_id) ON DELETE CASCADE;


--
-- Name: users users_user_groups_group_id_fk; Type: FK CONSTRAINT; Schema: user; Owner: postgres
--

ALTER TABLE ONLY "user".users
    ADD CONSTRAINT users_user_groups_group_id_fk FOREIGN KEY (group_id) REFERENCES "user".user_groups(group_id);


--
-- PostgreSQL database dump complete
--

