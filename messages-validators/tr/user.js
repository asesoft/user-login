'use strict';

const JoiError = require(appDir + '/library/joiError');
const joi = require('joi');
const messages = {};
const validators = {};

//----------------------------------------------------------------------------------------------------------------------
messages.emailAlreadyExists = (arg) => `${arg} başka bir hesap tarafından kullanılıyor`;
messages.badName = 'Adınız min 3 max 50 karakter olabilir';
messages.badSurname = 'Soyadınız min 3 max 50 karakter olabilir';
messages.badEmail = 'Email adresiniz doğru formatta değil';
messages.badPassword = 'Şifreniz min 4 max 32 karakter olabilir';
messages.invalidPassword = 'Mevcut şifreniz yanlış';
messages.badGAuth = 'gauth seçeneği yalnızca true yada false olabilir';
//----------------------------------------------------------------------------------------------------------------------

validators.update = joi.object({
    name: joi.string().min(3).max(50).error(new JoiError(messages.badName)),
    surname: joi.string().min(3).max(50).error(new JoiError(messages.badSurname)),
    email: joi.string().email().error(new JoiError(messages.badEmail)),
    password: joi.string().required().min(4).max(32).error(new JoiError(messages.badPassword)),
    new_password: joi.string().min(4).max(32).error(new JoiError(messages.badPassword)),
    gauth: joi.bool().error(new JoiError(messages.badGAuth))
});

module.exports = {messages, validators};