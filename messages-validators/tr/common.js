'use strict';

const messages = {};
const validators = {};
//----------------------------------------------------------------------------------------------------------------------
messages.serverError = 'Server Hatası';
messages.canNotSendEmail = 'Mail gönderilemedi';
messages.invalidToken = 'Token süreniz geçmiş yada geçersiz token';
//----------------------------------------------------------------------------------------------------------------------

module.exports = {messages, validators};