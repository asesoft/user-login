'use strict';

const JoiError = require(appDir + '/library/joiError');
const joi = require('joi');
const messages = {};
const validators = {};

//----------------------------------------------------------------------------------------------------------------------
messages.badEmail = 'Email adresi Hatalı';
messages.badPassword = 'Şifre en az 4 en fazla 32 karakter olmalıdır';
messages.emailNotfound = 'Email adresi bulunamadı';
messages.userNotFound = 'Üye Bulunamadı';
messages.incorrectPassword = 'Şifreniz yanlış';
messages.emailNotActive = 'Email adresiniz onaylı değil';
messages.badName = 'Adınız min 3 max 50 karakter olabilir';
messages.badSurname = 'Soy Adınız min 3 max 50 karakter olabilir';
messages.emailAlreadyExists = (arg) => `${arg.join(', ')} zaten kayıtlı`;
messages.foreignKey = (arg) => `${arg.join(', ')} foreign key hatası`;
messages.secretNotfound = 'Kullanıcı secret keyi bulunamadı';
messages.invalidGAuthCode = 'Kodunuz onaylanamadı. Süresi geçmiş yada geçersiz';
messages.tokenAlreadyExists = 'Bu token zaten kullanımda';
messages.emailCodeAlreadyExists = 'Bu email doğrulama kodu zaten kullanımda';
messages.emailVerifyMailTitle = 'Email adresi doğrulama';
messages.emailVerificationCodeIsInvalid = 'Email doğrulama kodu geçersiz';
messages.changePasswordCodeAlreadyExists = 'Bu email doğrulama kodu zaten kullanımda';
messages.changePasswordEmailTitle = 'Şifre Değiştirme linki';
messages.changePasswordCodeIsInvalid = 'Şifre değiştirme kodu geçersiz';

//----------------------------------------------------------------------------------------------------------------------

validators.login = joi.object({
    email: joi.string().required().email().error(new JoiError(messages.badEmail)),
    password: joi.string().required().min(4).max(32).error(new JoiError(messages.badPassword))
});

validators.register = joi.object({
    name: joi.string().required().min(3).max(50).error(new JoiError(messages.badName)),
    surname: joi.string().required().min(3).max(50).error(new JoiError(messages.badSurname)),
    email: joi.string().required().email().error(new JoiError(messages.badEmail)),
    password: joi.string().required().min(4).max(32).error(new JoiError(messages.badPassword))
});

validators.sendEmailVerificationCode = joi.string().required().email().error(new JoiError(messages.badEmail));
validators.verifyEmail = joi.string().max(128).min(1).required().error(new JoiError(messages.emailVerificationCodeIsInvalid));
validators.forgotPassword = joi.string().required().email().error(new JoiError(messages.badEmail));

validators.changePassword = joi.object({
    code: joi.string().max(128).min(1).required().error(new JoiError(messages.emailVerificationCodeIsInvalid)),
    password: joi.string().max(50).min(4).required().error(new JoiError(messages.badPassword))
});

//----------------------------------------------------------------------------------------------------------------------
module.exports = {messages, validators};