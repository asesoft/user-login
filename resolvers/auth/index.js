'use strict';
const logger = registry.get('logger');
const model = require(appDir + '/models/auth/auth');
const md5 = require('md5');
const {convertIP, renderMailBody, sendMail} = registry.get('helpers');
const speakeasy = require('speakeasy');
const uuid = require('uuid/v4');
const env = registry.get('env');

class Resolvers {

    static async login({email, password}, context){
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {

            await authValidators.login.validate({email, password});
            let user = await model.getUserAndSettingsByEmail(email);
            let hash = md5(md5(password));
            let token;

            if(!user){
                return new Error(authMessages.emailNotfound);
            }

            if(user.password !== hash){
                return new Error(authMessages.incorrectPassword);
            }

            if(user.email_active === false){
                return new Error(authMessages.emailNotActive);
            }

            if(user.gauth === false){
                token = await model.addAuthToken(user, IP, platform);
                return {
                    token,
                    user_id: user.user_id,
                    first_login: user.first_login,
                    auth_type: 'Basic'
                };
            }

            return {
                user_id: user.user_id,
                auth_type: 'GAuth'
            };

        } catch (e) {
            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }

    }


    static async register({name, surname, email, password}, context){

        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');

        try {

            await authValidators.register.validate({name, surname, email, password});
            let code = uuid();

            let result = await model.register(name, surname, email, md5(md5(password)), IP, platform, code);
            if(Reflect.has(result, 'isError')){
                if(result.type === 'unique'){
                    return new Error(authMessages.emailAlreadyExists(result.detail.map(fields => fields.value)));
                } else if (result.type === 'foreignKey'){
                    return new Error(authMessages.foreignKey(result.detail.map(fields => fields.column)));
                }
            }

            let mailBody = await renderMailBody(context.req.lang, 'account-email-verify.ejs', {code});
            sendMail(env.MAIL_FROM_NAME, env.MAIL_FROM_MAIL, email, authMessages.emailVerifyMailTitle, mailBody);

            return result;

        } catch (e) {
            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async genGAuthCode({user_id}, context){
        let {messages: authMessages} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');

        try {

            let secret = await model.getGAuthSecretByUser(user_id);
            if(!secret){
                return new Error(authMessages.secretNotfound);
            }

            speakeasy.totp({secret, encoding: 'base32'});
            return true;

        } catch (e) {
            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async verifyGAuthCode({user_id, code}, context){
        let {messages: authMessages} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {

            let us = await model.getUserAndSecret(user_id);

            if(!us){
                return new Error(authMessages.secretNotfound);
            }

            let verified = speakeasy.totp.verify({secret: us.secret, encoding: 'base32', token: code});

            if(!verified){
                return new Error(authMessages.invalidGAuthCode);
            }

            let token = await model.addAuthToken(us, IP, platform);
            if(Reflect.has(token, 'isError')){
                if(token.type === 'unique'){
                    return new Error(authMessages.tokenAlreadyExists(token.detail.map(fields => fields.value)));
                } else if (token.type === 'foreignKey'){
                    return new Error(authMessages.foreignKey(token.detail.map(fields => fields.column)));
                }
            }

            return {token, user_id: us.user_id, first_login: us.first_login, auth_type: 'GAuth'};

        } catch (e) {
            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async sendEmailVerificationCode({email}, context){
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {

            await authValidators.sendEmailVerificationCode.validate(email);
            let user = await model.getUserByEmail(email);
            if(!user){
                return new Error(authMessages.emailNotfound);
            }

            let code = uuid();
            let result = await model.addEmailVerificationCode(user.user_id,IP, platform, code);
            if(typeof result === 'object' && Reflect.has(result, 'isError')){
                if(result.type === 'unique'){
                    return new Error(authMessages.emailCodeAlreadyExists(result.detail.map(fields => fields.value)));
                } else if (result.type === 'foreignKey'){
                    return new Error(authMessages.emailNotfound);
                }
            }

            let mailBody = await renderMailBody(context.req.lang, 'account-email-verify.ejs', {code});
            let sending = await sendMail(env.MAIL_FROM_NAME, env.MAIL_FROM_MAIL, email, authMessages.emailVerifyMailTitle, mailBody);
            if(!sending){
                return new Error(commonMessages.canNotSendEmail);
            }

            return true;

        } catch (e) {

            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async verifyEmail({code}, context){
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {
            await authValidators.verifyEmail.validate(code);

            let user_id = await model.getUserIdByEmailCode(code);
            if(!user_id){
                return Error(authMessages.emailVerificationCodeIsInvalid);
            }

            if(!model.verifyEmail(user_id,IP, platform)){
                return Error(commonMessages.serverError);
            }

            return true;
        } catch (e) {
            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async forgotPassword({email}, context){
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {

            await authValidators.forgotPassword.validate({email});
            let user = await model.getUserByEmail(email);
            if(!user){
                return new Error(authMessages.emailNotfound);
            }

            let code = uuid();
            let result = await model.addChangePasswordCode(user.user_id, IP, platform, code);
            if(typeof result === 'object' && Reflect.has(result, 'isError')){
                if(result.type === 'unique'){
                    return new Error(authMessages.changePasswordCodeAlreadyExists(result.detail.map(fields => fields.value)));
                } else if (result.type === 'foreignKey'){
                    return new Error(authMessages.emailNotfound);
                }
            }

            let mailBody = await renderMailBody(context.req.lang, 'account-change-password.ejs', {code});
            let sending = await sendMail(env.MAIL_FROM_NAME, env.MAIL_FROM_MAIL, email, authMessages.changePasswordEmailTitle, mailBody);
            if(!sending){
                return new Error(commonMessages.canNotSendEmail);
            }

            return true;

        } catch (e) {

            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async changePassword({code, password}, context){
        let {messages: authMessages, validators: authValidators} = context.MVS.get('auth');
        let {messages: commonMessages} = context.MVS.get('common');
        let IP = convertIP(context.req.connection.remoteAddress);
        let platform = context.req.useragent;

        try {
            await authValidators.changePassword.validate({code, password});

            let user_id = await model.getUserIdByCPCode(code);
            if(!user_id){
                return Error(authMessages.changePasswordCodeIsInvalid);
            }

            if(!model.changePassword(user_id, md5(md5(password)),IP, platform)){
                return Error(commonMessages.serverError);
            }

            return true;
        } catch (e) {
            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.log('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async isLogged(args, context){
        return !!context.user;
    }

}

module.exports = Resolvers;