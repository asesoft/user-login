'use strict';

const { GraphQLScalarType } =  require('graphql');
const moment = require('moment');

const config = {
    name: 'Date',
    description: 'Date type',

    serialize(value) {
        return moment(value).format('YYYY-MM-DD').toString();
    },

    parseValue(value) {
        return String(value);
    },

    parseLiteral(ast) {
        if(!moment(ast.value, 'YYYY-MM-DD', true).isValid()){
            throw new Error('Girilen veri tipi datetime ve YYYY-MM-DD formatında olmalıdır');
        }

        return moment(ast.value, 'YYYY-MM-DD', true).toDate();
    }
};

module.exports = new GraphQLScalarType(config);