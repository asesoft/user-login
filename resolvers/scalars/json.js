'use strict';

const { GraphQLScalarType } =  require('graphql');

const config = {
    name: 'JSON',
    description: 'JSON type',

    serialize(value) {
        return value;
    },

    parseValue(value) {
        return String(value);
    },

    parseLiteral(ast) {
        try {
            return JSON.parse(ast.value);
        } catch (e) {
            throw new Error('Girilen veri tipi JSON formatında olmalıdır');
        }
    }
};

module.exports = new GraphQLScalarType(config);