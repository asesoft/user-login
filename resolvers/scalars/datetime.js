'use strict';
const { GraphQLScalarType } =  require('graphql');
const moment = require('moment');

const config = {
    name: 'DateTime',
    description: 'Datetime type',

    serialize(value) {
        return moment(value).format('YYYY-MM-DD hh:mm:ss').toString();
    },

    parseValue(value) {
        return String(value);
    },

    parseLiteral(ast) {
        if(!moment(ast.value, 'YYYY-MM-DD hh:mm:ss', true).isValid()){
            throw new Error(' Girilen veri tipi datetime ve YYYY-MM-DD hh:mm:ss formatında olmalıdır');
        }

        return moment(ast.value, 'YYYY-MM-DD hh:mm:ss', true).toDate();
    }
};

module.exports = new GraphQLScalarType(config);