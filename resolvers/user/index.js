'use strict';
const {fieldStacker} = registry.get('helpers');
const model = require(appDir + '/models/user/user');
const logger = registry.get('logger');
const md5 = require('md5');

class Resolvers {

    static async userInfo(args, context){
        return await model.getUserInfo(context.user.user_id);
    }

    static async update({fields}, context){
        let users = {name: 'users', fields: ['name', 'surname', 'email', 'password', 'new_password']};
        let user_settings = {name: 'user_settings', fields: ['gauth']};
        let grouped = fieldStacker(fields, users, user_settings);
        let {messages: userMessages, validators: userValidators} = context.MVS.get('user');
        let {messages: commonMessages} = context.MVS.get('common');

        try {
            await userValidators.update.validate(fields);
            if(md5(md5(fields.password)) !== context.user.password){
                return new Error(userMessages.invalidPassword);
            }

            let result = await model.update(context.user, grouped);
            if(typeof result === 'object' && Reflect.has(result, 'isError')){
                if(result.type === 'unique'){
                    return new Error(userMessages.emailAlreadyExists(fields.email));
                }
            }

            return fields;

        } catch (e) {
            if(e.isJoi){
                throw new Error(e.message);
            }

            logger.error('error', e.message);
            throw new Error(commonMessages.serverError);
        }
    }

    static async filtered({filter, orders, cursor}, context,){
        await model.deneme(filter, orders, cursor);
        return true;
    }
}

module.exports = Resolvers;