'use strict';
const {resolversHelpers: { checkLogin }} = registry.get('helpers');
const auth = require('./auth/index');
const user = require('./user/index');
const dateTime = require('./scalars/datetime');
const date = require('./scalars/date');
const json = require('./scalars/json');

const resolvers = {

    Query: {
        auth: () => ({
            isLogged: auth.isLogged
        }),

        user: (parent, args, context) => {
            checkLogin(context);
            return {
                userInfo: user.userInfo,
                filtered: user.filtered
            };
        }
    },

    Mutation: {

        auth: () => ({
            register: auth.register,
            changePassword: auth.changePassword,
            login: auth.login,
            genGAuthCode: auth.genGAuthCode,
            verifyGAuthCode: auth.verifyGAuthCode,
            sendEmailVerificationCode: auth.sendEmailVerificationCode,
            verifyEmail: auth.verifyEmail,
            forgotPassword: auth.forgotPassword
        }),

        user: (parent, args, context) => {
            checkLogin(context);
            return {
                update: user.update
            };
        }
    },

    Datetime: dateTime,
    Date: date,
    Json: json
};

module.exports = resolvers;
