'use strict';

class Pagination {

    constructor(){

        this.prevName = '< önceki sayfa';
        this.nextName = 'sonraki Sayfa >';
        this.lastName = 'son sayfa >>';
        this.firstName = '<< ilk sayfa';
        this.pageLength = 5;
        this.currentPage = 0;
        this.limit = 0;
        this.totalRow = 0;
        this.startRow = 0;
        this.lastP = 0;
        this.cursorObj = {};
        this.cursorStr = '';
        this.cursorHandler = {};
    }

    static genCursorString(obj){
        return Buffer.from(JSON.stringify(obj)).toString('base64');
    }

    static setTypes(types){
        this.prototype.types = types;
    }

    setPrevName(name){
        this.prevName = name;
    }

    setNextName(name){
        this.nextName = name;
    }

    setLastName(name){
        this.lastName = name;
    }

    setFirstName(name){
        this.firstName = name;
    }

    setPageLength(length){
        this.pageLength = parseInt(length);
    }

    setTotalRow(total){
        total = parseInt(total);
        if(isNaN(total)){
            total = 0;
        }

        this.totalRow = total;
    }

    getTotalRow(){
        return this.totalRow;
    }

    getStartRow(){
        return this.startRow;
    }

    getLimit() {
        return this.limit;
    }

    setCurrentPage(page){
        page = parseInt(page);
        if(isNaN(page)){
            page = 0;
        }

        this.currentPage = page;
    }

    getCurrentPage() {
        return this.currentPage;
    }

    setLimit(limit){
        limit = parseInt(limit);
        if(isNaN(limit)){
            limit = 10;
        }

        this.limit = limit;
    }

    getLastP(){
        return this.lastP;
    }


    setCursorHandler(cHandler){
        if(!(cHandler instanceof this.types.CursorHandler)){
            throw new Error('Cursor Handler bir "CursorHandler" türevi olmalıdır');
        }

        this.cursorHandler = cHandler;
    }

    setCursor(cursorStr){
        this.cursorStr = cursorStr;
        if(cursorStr.match(/^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/g) == null){
            throw new Error('cursor doğru formatta değil');
        }

        let decode = Buffer.from(cursorStr, 'base64').toString('utf8');
        this.cursorObj = JSON.parse(decode);
    }

    getCursor(){
        return this.cursorObj;
    }

    init(){
        this.lastP = Math.ceil(this.totalRow / this.limit);
        if(this.currentPage < 1){
            this.currentPage = 1;
        } else if (this.currentPage > this.lastP){
            this.currentPage = this.lastP;
        }
    }

    async smartRender(){

        let pages = [];
        let cursors = [];

        if(this.totalRow > this.limit && this.currentPage > 0 && this.currentPage <= this.lastP ){
            if(this.currentPage > 1){
                pages.push({name: this.firstName, value: 1, current: false});
                cursors.push(typeof this.cursorHandler.first == 'function' ? this.cursorHandler.first(1) : {});

                pages.push({name: this.prevName, value: this.currentPage - 1, current: false});
                cursors.push(typeof this.cursorHandler.prev == 'function' ? this.cursorHandler.prev(this.currentPage  - 1) : {});
            }

            let prevPages = [];
            let prevCursors =  [];

            for(let prev = this.currentPage - 1; prev >=  (this.currentPage - this.pageLength <= 0 ? 1 : this.currentPage - this.pageLength); prev--){
                prevPages.push({name: prev, value: prev, current: false});
                prevCursors.push(typeof this.cursorHandler.prevSeq == 'function' ? this.cursorHandler.prevSeq(prev) : {});
            }

            prevPages.reverse();
            prevCursors.reverse();

            pages = [...pages, ...prevPages];
            cursors = [...cursors, ...prevCursors];

            pages.push({name: this.currentPage, value: this.currentPage, current: true});
            cursors.push(typeof this.cursorHandler.current == 'function' ? this.cursorHandler.current(this.currentPage) : {});

            for(let next = this.currentPage + 1; next <= this.currentPage + this.pageLength && next < this.lastP; next++){
                pages.push({name: next, value: next, current: false});
                cursors.push(typeof this.cursorHandler.nextSeq == 'function' ? this.cursorHandler.nextSeq(next) : {});
            }

            if(this.currentPage < this.lastP){
                pages.push({name: this.nextName, value: this.currentPage + 1, current: false});
                cursors.push(typeof this.cursorHandler.next == 'function' ? this.cursorHandler.next(this.currentPage + 1) : {});

                pages.push({name: this.lastName, value: this.lastP, current: false});
                cursors.push(typeof this.cursorHandler.last == 'function' ? this.cursorHandler.last(this.lastP) : {});
            }

            let cResults = await Promise.all(cursors);
            for(let i = 0; i < cResults.length; i++){
                if(typeof cResults[i] !== 'object'){
                    throw new Error('cursor handler geriye obje döndürmelidir');
                }
                pages[i].cursor = this.constructor.genCursorString(cResults[i]);
            }
        }

        return pages;
    }

    async shortRender() {

        let pages = [];
        let cursors = [];

        if(this.totalRow > this.limit && this.currentPage > 0 && this.currentPage <= this.lastP){
            if(this.currentPage > 1){
                pages.push({name: this.firstName, value: 1, current: false});
                cursors.push(typeof this.cursorHandler.first == 'function' ? this.cursorHandler.first(1) : {});

                pages.push({name: this.prevName, value: this.currentPage - 1, current: false});
                cursors.push(typeof this.cursorHandler.prev == 'function' ? this.cursorHandler.prev(this.currentPage  - 1) : {});
            }

            pages.push({name: this.currentPage + ' / ' + this.lastP, value: this.currentPage, current: true});
            cursors.push(typeof this.cursorHandler.current == 'function' ? this.cursorHandler.current(this.currentPage) : {});

            if(this.currentPage < this.lastP){
                pages.push({name: this.nextName, value: this.currentPage + 1, current: false});
                cursors.push(typeof this.cursorHandler.next == 'function' ? this.cursorHandler.next(this.currentPage + 1) : {});

                pages.push({name: this.lastName, value: this.lastP, current: false});
                cursors.push(typeof this.cursorHandler.last == 'function' ? this.cursorHandler.last(this.lastP) : {});
            }

            let cResults = await Promise.all(cursors);
            for(let i = 0; i < cResults.length; i++){
                pages[i].cursor = this.constructor.genCursorString(cResults[i]);
            }
        }

        return pages;
    }

}

module.exports = Pagination;
