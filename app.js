'use strict';

/*
 *root klasörü globale atıyoruz böylece her modülden erişilebilir
 *registry set edilen değişkenlere erişimi sağlar
 */

global.appDir = __dirname;
global.registry = require(__dirname + '/core/registry');

//--------------------------------------------------------------------------------------------------------------------//

const env = require('dotenv').config().parsed;
const requireDir = require('require-dir');
const config = require(appDir + '/config/config');
const winston = require('winston');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const { ApolloServer } = require('apollo-server-express');
const { importSchema } = require('graphql-import');
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: __dirname + '/logs/combined.log' })
    ]
});

//----------------------------------------------------------------------------------------------------------------------

/*
 * formatError ürün canlıya alındığında hataların stacklerini siler
 */

function formatError(error) {
    if(env.NODE_ENV === 'production'){
        delete error.extensions.exception;
        return error;
    }

    return error;
}


/*
 * init fonksiyonu başlatıcı fonksiyondur
 */

async function init(){
    //------------------------------------------------------------------------------------------------------------------
    let knex = require('knex')({
        client: 'pg',
        connection: env.DB_CONNECTION_STRING,
        pool: {
            min: parseInt(env.DB_MIN_POOL),
            max: parseInt(env.DB_MAX_POOL)
        }
    });

    //------------------------------------------------------------------------------------------------------------------
    //registry.set('pubsub', new PubSub());
    registry.set('knex', knex);
    registry.set('logger', logger);
    registry.set('config', config);
    registry.set('app', app);
    registry.set('env', env);
    //registry.set('BaseResolver', require(appDir + '/core/BaseResolver'));
    registry.set('helpers', requireDir(appDir + '/helpers', {recurse: true}));

    //------------------------------------------------------------------------------------------------------------------
    const { loader } = registry.get('helpers');
    (new loader(app, appDir + '/middlewares/app-level')).middlewares();
    (new loader(app, appDir + '/middlewares/router-level')).routers();
    (new loader(app, appDir + '/routes')).routers();
    //------------------------------------------------------------------------------------------------------------------

    const typeDefs = importSchema(__dirname + '/schemes/typeDefs.graphql');
    const resolvers = require(__dirname + '/resolvers/index');
    const server = new ApolloServer({ typeDefs, resolvers, formatError, context: require(__dirname + '/context/context') });
    //------------------------------------------------------------------------------------------------------------------
    server.applyMiddleware({app, path: '/graphql'});
    app.use('/public', express.static(__dirname + '/public'));
    app.set('trust proxy', 1);

    //------------------------------------------------------------------------------------------------------------------

    registry.set('server', server);

    //------------------------------------------------------------------------------------------------------------------
    http.listen(env.LISTEN_PORT, env.LISTEN_HOST);
    return true;
}

init().then(() => logger.info(`${env.LISTEN_HOST}:${env.LISTEN_PORT} ip adresini dinliyor`));