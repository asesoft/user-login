'use strict';

const knex = registry.get('knex');
const moment = require('moment');

class Context {

    static async getUserInfo(token){
        return await knex
            .table({at: 'user.auth_tokens'})
            .innerJoin({u: 'user.users'}, 'at.user_id', '=', 'u.user_id')
            .innerJoin({ug: 'user.user_groups'}, 'u.group_id', '=', 'ug.group_id')
            .where('at.token', '=', token)
            .where('at.expiry_date', '>', moment().utc().toDate())
            .select({
                user_id: 'u.user_id', name: 'u.name', surname: 'u.surname', password: 'u.password',
                group_id: 'u.group_id', group_name: 'ug.name', email: 'u.email', group_level: 'ug.level',
                user_permissions: 'ug.permissions'
            })
            .first();
    }
}

module.exports = Context;