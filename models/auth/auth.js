'use strict';

const {errorParser, transaction} = registry.get('helpers');
const logger = registry.get('logger');
const knex = registry.get('knex');
const speakeasy = require('speakeasy');
const qrcode = require('qrcode');
const moment = require('moment');
const uuid = require('uuid/v4');

class Auth {


    static async getUserAndSettingsByEmail(email){
        return await knex
            .table({u: 'user.users'})
            .leftJoin({us: 'user.user_settings'}, 'us.user_id', '=', 'u.user_id')
            .where('u.email', '=', email)
            .select('u.user_id', 'u.password', 'us.email_active', 'us.first_login', 'us.gauth')
            .limit(1)
            .first();
    }

    static async getUserByEmail(email){
        return await knex
            .table('user.users')
            .select('user_id', 'name', 'surname').where('email', '=', email)
            .limit(1)
            .first();
    }

    static async getUserIdByEmailCode(code){
        let result = await knex
            .table({evc: 'user.email_verification_codes'})
            .select('u.user_id')
            .innerJoin({u: 'user.users'}, 'evc.user_id', '=', 'u.user_id')
            .where('evc.code', '=', code)
            .limit(1)
            .first();

        return result !== undefined ? result.user_id : undefined;
    }

    static async getUserIdByCPCode(code){
        let result = await knex
            .table({cpc: 'user.change_password_codes'})
            .select('u.user_id')
            .innerJoin({u: 'user.users'}, 'cpc.user_id', '=', 'u.user_id')
            .where('cpc.code', '=', code)
            .limit(1)
            .first();

        return result !== undefined ? result.user_id : undefined;
    }

    static async getUserAndSecret(user_id){
        return await knex
            .table({u: 'user.users'})
            .innerJoin({us: 'user.user_settings'}, 'us.user_id', '=', 'u.user_id')
            .innerJoin({s: 'user.gauth_secrets'}, 's.user_id', '=', 'u.user_id')
            .select('u.user_id', 'us.email_active', 'us.first_login', 's.secret')
            .where('u.user_id', '=', user_id)
            .limit(1)
            .first();
    }

    static async getGAuthSecretByUser(user_id){
        let result = await knex
            .table('user.gauth_secrets')
            .select('secret')
            .where('user_id', '=', user_id)
            .limit(1)
            .first();

        return result !== undefined ? result.secret: undefined;
    }


    static async register(name, surname, email, password, IP, platform, code){
        let trx = await transaction();
        try {

            let secret = speakeasy.generateSecret({length: 20});
            let qr = await qrcode.toDataURL(secret.base32);

            let user = (await trx
                .table('user.users').insert({group_id: 3, name, surname, email, password})
                .returning(['user_id', 'name', 'surname', 'email'])
            )[0];

            await trx.table('user.gauth_secrets').insert({user_id: user.user_id, secret: secret.base32, qr_code: qr});
            await trx
                .table('system.logs')
                .insert({user_id: user.user_id, type: 1, ip: IP, date: moment().utc().toDate(), platform: platform});
            await trx.table('user.email_verification_codes').insert({user_id: user.user_id, code});
            await trx.commit();

            return user;

        } catch (e) {
            await trx.rollback();
            let err = errorParser(e.code, e.detail);
            if(err.type !== undefined){
                return err;
            }

            throw e;
        }
    }

    static async addAuthToken(user, IP, platform){
        let trx = await transaction();
        let token = uuid();

        try {
            await trx
                .table('user.auth_tokens')
                .insert({user_id: user.user_id, token, expiry_date: moment().utc().add(14, 'days').toDate()});

            await trx
                .table('system.logs')
                .insert({user_id: user.user_id, type: 2, ip: IP, date: moment().utc().toDate(), platform: platform});

            if(user.first_login === false){
                await trx.table('user.user_settings').where('user_id', '=', user.user_id).update({first_login: true });
            }

            await trx.commit();
            return token;

        } catch (e) {
            await trx.rollback();
            let err = errorParser(e.code, e.detail);
            if(err.type !== undefined){
                return err;
            }

            throw e;
        }
    }

    static async addEmailVerificationCode(user_id, IP, platform, code){
        let trx = await transaction();
        try {
            await trx.table('user.email_verification_codes').insert({user_id, code});
            await trx
                .table('system.logs')
                .insert({user_id, type: 3, ip: IP, date: moment().utc().toDate(), platform: platform});

            await trx.commit();
            return true;
        } catch (e) {
            await trx.rollback();
            let err = errorParser(e.code, e.detail);
            if(err.type !== undefined){
                return err;
            }

            throw e;
        }
    }

    static async verifyEmail(user_id, IP, platform){
        let trx = await transaction();

        try {
            await trx.table('user.user_settings').where('user_id', '=', user_id).update({email_active: true});
            await trx.table('user.email_verification_codes').where('user_id', '=', user_id).delete();
            await trx
                .table('system.logs')
                .insert({user_id, type: 4, ip: IP, date: moment().utc().toDate(), platform: platform});

            await trx.commit();
            return true;
        } catch (e) {
            await trx.rollback();
            logger.error(e.message);
            return false;
        }
    }

    static async addChangePasswordCode(user_id, IP, platform, code) {
        let trx = await transaction();

        try {
            await trx.table('user.change_password_codes').insert({user_id, code});
            await trx
                .table('system.logs')
                .insert({user_id, type: 5, ip: IP, date: moment().utc().toDate(), platform: platform});

            await trx.commit();
            return true;
        } catch (e) {
            await trx.rollback();
            let err = errorParser(e.code, e.detail);
            if (err.type !== undefined) {
                return err;
            }

            throw e;
        }
    }

    static async changePassword(user_id, password, IP, platform){
        let trx = await transaction();

        try {
            await trx.table('user.users').where('user_id', '=', user_id).update({password: password});
            await trx.table('user.change_password_codes').where('user_id', '=', user_id).delete();
            await trx
                .table('system.logs')
                .insert({user_id, type: 6, ip: IP, date: moment().utc().toDate(), platform: platform});

            await trx.commit();
            return true;
        } catch (e) {
            await trx.rollback();
            logger.error(e.message);
            return false;
        }
    }

}

module.exports = Auth;