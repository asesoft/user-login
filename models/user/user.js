'use strict';
const knex = registry.get('knex');
const {transaction, errorParser, filterAndSorter} = registry.get('helpers');
const logger = registry.get('logger');
const md5 = require('md5');

class User {

    static async getUserInfo(user_id) {
        return await knex
            .table({u: 'user.users'})
            .innerJoin({us: 'user.user_settings'}, 'u.user_id', '=', 'us.user_id')
            .innerJoin({gs: 'user.gauth_secrets'}, 'u.user_id', '=', 'gs.user_id')
            .where('u.user_id', '=', user_id)
            .select(
                'u.user_id', 'u.name', 'u.surname', 'u.email', 'us.email_active', 'us.first_login', 'us.gauth',
                'gs.qr_code'
            )
            .limit(1)
            .first();
    }

    static async update(user, fields){

        if(fields.users !== undefined){
            if(fields.users.email !== undefined && fields.users.email !== user.email){
                fields.user_settings = {...fields.user_settings, email_active: false};
            }

            delete fields.users.password;

            if(fields.users.new_password !== undefined){
                fields.users.password = md5(md5(fields.users.new_password));
                delete fields.users.new_password;
            }
        }

        let trx = await transaction();

        try {
            if(fields.users !== undefined){
                await trx.table('user.users').where('user_id', '=', user.user_id).update(fields.users);
            }

            if(fields.user_settings !== undefined){
                await trx.table('user.user_settings').where('user_id', '=', user.user_id).update(fields.user_settings);
            }

            await trx.commit();
            return true;

        } catch (e) {
            await trx.rollback();
            logger.error(e.message);
            let err = errorParser(e.code, e.detail);
            if (err.type !== undefined) {
                return err;
            }

            throw e;
        }
    }


    static async deneme(filter, orders, cursor){

        let field2CB = (tmpKnex, operator, value) => {
            switch(operator){
                case 'eq':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).where('fiyat', value));
                    tmpKnex.orWhere((qb) => qb.where('i_fiyat', value));
                    break;
                case 'ne':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).whereNot('fiyat', value));
                    tmpKnex.orWhere((qb) => qb.whereNot('i_fiyat', value));
                    break;
                case 'in':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).whereIn('fiyat', value));
                    tmpKnex.orWhere((qb) => qb.whereIn('i_fiyat', value));
                    break;

                case 'notIn':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).whereNotIn('fiyat', value));
                    tmpKnex.orWhere((qb) => qb.whereNotIn('i_fiyat', value));
                    break;
                case 'gt':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).where('fiyat', '>', value));
                    tmpKnex.orWhere((qb) => qb.where('i_fiyat','>',  value));
                    break;

                case 'lt':
                    tmpKnex.where((qb) => qb.where('i_fiyat', 0.00).where('fiyat', '<', value));
                    tmpKnex.orWhere((qb) => qb.where('i_fiyat','<',  value));
                    break;

            }

            return tmpKnex;
        };

        let tables = {
            filter: {
                field1: {
                    from: '"users"',
                    alias: `CASE WHEN "user"."users"."name" IS NOT NULL THEN "user"."users"."name" ELSE "user"."users"."surname" END`
                },
                field2: {
                    //from: '"users"'
                    callback: field2CB
                },
                field3: {
                    from: '"user2"'
                }
            },
            sort: {
                field4: {
                    from: '"users"',
                    alias: `CASE WHEN "user"."users"."name" IS NOT NULL THEN "user"."users"."name" ELSE "user"."users"."name" END`
                },
                field5: {
                    from: '"users"'
                },
                field6: {
                    from: '"users"'
                }
            }
        };

        let kn = knex.table('users').select('user_bir', 'iki', 'uc').where('user_id', '=', 1);
        kn = filterAndSorter(kn, filter, orders, cursor, tables);

        console.log(kn.toString());
    }

}

module.exports = User;