'use strict';
const model = require(appDir + '/models/context/context');
const MVS = require('./MVS');

async function context({ req }) {

    let data = {};
    //------------------------------------------------------------------------------------------------------------------
    data.user = undefined;
    if(typeof req.get('X-TOKEN') !== 'undefined'){
        data.user = await model.getUserInfo(req.get('X-TOKEN'));
    }

    //------------------------------------------------------------------------------------------------------------------
    data.req = req;
    //------------------------------------------------------------------------------------------------------------------
    data.MVS = new MVS(req.lang);
    //------------------------------------------------------------------------------------------------------------------
    return data;
}

module.exports = context;