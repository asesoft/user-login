'use strict';
const knex = registry.get('knex');

function transaction(){
    return new Promise((resolve, reject) => knex.transaction(trx => resolve(trx)).catch(err => reject(err)));
}

module.exports = transaction;