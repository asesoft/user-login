'use strict';
const numbers = [0,1,2,3,4,5,6,7,8,9];

function randomCode(min, max){
    let tmp = [];
    let randStep = Math.floor(Math.random() * (max - min) ) + min;
    for(let i = 0; i <= randStep; i++){
        tmp.push(Math.floor(Math.random() * numbers.length));
    }

    return tmp.join('');
}

module.exports = randomCode;