'use strict';

const DataLoader = require('dataloader');

function createDataloader(fn){
    return new DataLoader(keys => {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await fn(keys);
                resolve(result);
            } catch (e){
                reject(e);
            }
        })
    })
}

module.exports = createDataloader;