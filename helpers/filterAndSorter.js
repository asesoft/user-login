'use strict';

/*
    örnek fields şeması
    {
        field: {from: "tabloAdı", alias: "x + y", sql: "ali = ? AND veli = ?"}
    }
 */

const knex = registry.get('knex');

function filterBuilder(tmpKnex, filter, fields){

    //------------------------------------------------------------------------------------------------------------------
    for (let key in filter){
        if(key === 'AND'){
            tmpKnex = tmpKnex.where((builder) => {
                for(let subKey in filter[key]){
                    builder = filterBuilder(builder, filter[key][subKey], fields);
                }
            });
            continue;
        } else if (key === 'OR'){
            tmpKnex = tmpKnex.orWhere((builder) => {
                for(let subKey in filter[key]){
                    builder = filterBuilder(builder, filter[key][subKey], fields);
                }
            });

            continue;
        }

        let whereColumn = '';
        for (let operator in filter[key]){
            if(fields[key] === undefined){
                break;
            }

            if(fields[key].from !== undefined){
                whereColumn = `${fields[key].from}."${key}"`;
            } else if(fields[key].alias){
                whereColumn = `(${fields[key].alias})`;
            } else if(fields[key].callback){
                whereColumn = fields[key].callback;
            } else {
                break;
            }

            if(typeof whereColumn === 'function'){
                tmpKnex.where((qb) => whereColumn(qb, operator, filter[key][operator]));
                continue;
            }

            switch (operator) {
                case 'eq':
                    tmpKnex = tmpKnex.where(knex.raw(whereColumn), '=', filter[key][operator]);
                    break;

                case 'ne':
                    tmpKnex = tmpKnex.whereNot(knex.raw(whereColumn), filter[key][operator]);
                    break;

                case 'in':
                    tmpKnex = tmpKnex.whereIn(knex.raw(whereColumn), filter[key][operator]);
                    break;

                case 'notIn':
                    tmpKnex = tmpKnex.whereNotIn(knex.raw(whereColumn), filter[key][operator]);
                    break;
                case 'contains':
                    tmpKnex = tmpKnex.where(knex.raw(whereColumn), 'LIKE', filter[key][operator]);
                    break;

                case 'gt':
                    tmpKnex = tmpKnex.where(knex.raw(whereColumn), '>', filter[key][operator]);
                    break;

                case 'lt':
                    tmpKnex = tmpKnex.where(knex.raw(whereColumn), '<', filter[key][operator]);
                    break;
            }
        }
    }

    return tmpKnex;
}


function orderBuilder(tmpKnex, orders, cursor, fields){

    let orderedKeys = Object.keys(orders);
    tmpKnex.where((builder) => {
        for(let key of orderedKeys){
            let keyIndex = orderedKeys.indexOf(key);
            let whereColumn = '';

            if(fields[key] === undefined){
                break;
            }

            if(fields[key].from !== undefined){
                whereColumn = `${fields[key].from}."${key}"`;
            } else if(fields[key].alias){
                whereColumn = `(${fields[key].alias})`;
            } else {
                break;
            }

            if(keyIndex === 0){
                if(cursor[key] !== undefined){
                    builder = builder.where(knex.raw(whereColumn), (orders[key] === 'ASC' ? '>' : '<'), cursor[key]);
                }

                continue;
            }

            let beforeKeys = orderedKeys.slice(0, keyIndex + 1);
            builder = builder.orWhere((qb) => {
                let bWhereColumn = '';
                for (let beforeKey of beforeKeys){
                    if(fields[beforeKey] === undefined){
                        break;
                    }

                    if(fields[beforeKey].from !== undefined){
                        bWhereColumn = `${fields[beforeKey].from}."${beforeKey}"`;
                    } else if(fields[beforeKey].alias){
                        bWhereColumn = `(${fields[beforeKey].alias})`;
                    } else {
                        break;
                    }

                    if(cursor[beforeKey] !== undefined){
                        if(key !== beforeKey){
                            qb = qb.where(knex.raw(bWhereColumn), '=', cursor[beforeKey]);
                            continue;
                        }

                        qb = qb.where(knex.raw(bWhereColumn), (orders[beforeKey] === 'ASC' ? '>' : '<'), cursor[key]);
                    }
                }
            });
        }
    });

    let whereColumn = '';

    for (let key of orderedKeys){
        if(fields[key] === undefined){
            break;
        }

        if(fields[key].from !== undefined){
            whereColumn = `${fields[key].from}."${key}"`;
        } else if(fields[key].alias){
            whereColumn = `(${fields[key].alias})`;
        } else {
            break;
        }

        tmpKnex = tmpKnex.orderBy(knex.raw(whereColumn), orders[key]);
    }

    return tmpKnex;
}



function filterAndSorter(knexObj, filter, orders, cursorString, fields){

    if(typeof filter !== 'object'){
        return knexObj;
    }

    if(typeof fields !== 'object'){
        return knexObj;
    }

    if(fields['sort'] === undefined || fields['filter'] === undefined){
        throw new Error('fields içinde sort ve filter adında iki alt obje blunmalı');
    }

    let tmpKnex = knexObj;
    let cursor = {};
    if(cursorString){
        cursor = JSON.parse(Buffer.from(cursorString, 'base64'));
        for(let key in orders){
            if(cursor[key] === undefined){
                throw new Error('cursor içinde orders elemanları bulunmalı');
            }
        }
    }

    tmpKnex = tmpKnex.where((builder) => filterBuilder(builder, filter, fields.filter));
    tmpKnex = orderBuilder(tmpKnex, orders, cursor, fields.sort);

    return tmpKnex;
}

module.exports = filterAndSorter;