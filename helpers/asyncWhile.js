'use strict';

async function asyncWhile (res, callback) {

    if(typeof callback !== 'function'){
        throw new Error('callback fonksiyon olmalıdır');
    }

    if(typeof res === 'undefined' || res == null || res === false){
        return;
    }

    try{

        let result = await callback(res);
        if(typeof result === 'undefined' || result == null || result === false){
            return;
        }

        return new Promise((resolve) => {
            process.nextTick(() => {
                resolve(asyncWhile(result, callback));
            });
        });

    } catch (e) {
        throw e;
    }
}

module.exports = asyncWhile;