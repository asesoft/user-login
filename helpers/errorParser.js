'use strict';

const pattern = new RegExp('([^\\(\\)\\=\\"]+)\\)\\=\\(([^\\(\\)\\=\\"]+)','ig');
const errCodes = {
    23505: 'unique',
    23503: 'foreignKey'
};

function errorParser(errCode, detail){
    let m;
    let results = [];

    while ((m = pattern.exec(detail)) !== null) {
        if (m.index === pattern.lastIndex) {
            pattern.lastIndex++;
        }

        results.push({column: m[1], value: m[2]});
    }

    return {isError: true, type: typeof errCodes[errCode] !== 'undefined' ? errCodes[errCode] : undefined, detail: results};
}

module.exports = errorParser;