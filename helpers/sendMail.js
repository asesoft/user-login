'use strict';
const env = registry.get('env');
const logger = registry.get('logger');
const nodeMailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
/*
const transporter = nodeMailer.createTransport(smtpTransport({
    host: String(env.MAIL_HOST),
    port: parseInt(env.MAIL_PORT),
    auth: {
        user: String(env.MAIL_USER),
        pass: smtpPassword(String(env.MAIL_PASSWORD))
    },
    secure: true
}));
*/
const transporter = nodeMailer.createTransport(smtpTransport({
    service: 'SES',
    auth: {
        user: String(env.MAIL_USER),
        pass: String(env.MAIL_PASSWORD)
    }
}));


async function sendMail(from, fromMail, to, title, content){

    const mailOptions = {
        from: `${from} <${fromMail}>`,
        to: to,
        subject: title,
        html: content
    };

    return new Promise(resolve => {
        transporter.sendMail(mailOptions, (error) => {
            if (error) {
                logger.error(`mail gönderilemedi ${error}`);
                resolve(false);
                return;
            }
            resolve(true);
        });
    });
}

module.exports = sendMail;